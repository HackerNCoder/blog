title: COVID-19’s impact on Tor
---
pub_date: 2020-04-17
---
author: isabela
---
_html_body:

<p style="line-height:1.38"><span style="font-size:11pt; font-variant:normal; white-space:pre-wrap"><span style="font-family:Arial"><span style="color:#000000"><span style="font-weight:400"><span style="font-style:normal"><span style="text-decoration:none">Tor, like much of the world, has been caught up in the COVID-19 crisis. Like many other nonprofits and small businesses, the crisis has hit us hard, and we have had to make some difficult decisions. </span></span></span></span></span></span></p>
<p style="line-height:1.38"><span style="font-size:11pt; font-variant:normal; white-space:pre-wrap"><span style="font-family:Arial"><span style="color:#000000"><span style="font-weight:400"><span style="font-style:normal"><span style="text-decoration:none">We had to let go of 13 great people who helped make Tor available to millions of people around the world. We will move forward with a core team of 22 people, and remain dedicated to continuing our work on Tor Browser and the Tor software ecosystem.</span></span></span></span></span></span></p>
<p style="line-height:1.38"><span style="font-size:11pt; font-variant:normal; white-space:pre-wrap"><span style="font-family:Arial"><span style="color:#000000"><span style="font-weight:400"><span style="font-style:normal"><span style="text-decoration:none">The world won’t be the same after this crisis, and the need for privacy and secure access to information will become more urgent. In these times, being online is critical and many people face ongoing obstacles to getting and sharing needed information. We are taking today’s difficult steps to ensure the Tor Project continues to exist and our technology stays available.</span></span></span></span></span></span></p>
<p style="line-height:1.38"><span style="font-size:11pt; font-variant:normal; white-space:pre-wrap"><span style="font-family:Arial"><span style="color:#000000"><span style="font-weight:400"><span style="font-style:normal"><span style="text-decoration:none">We are terribly sad to lose such valuable teammates, and we want to let all our users and supporters know that Tor will continue to provide privacy, security, and censorship circumvention services to anyone who needs them.</span></span></span></span></span></span></p>
<!--break-->
