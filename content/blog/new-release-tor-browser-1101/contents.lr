title: New Release: Tor Browser 11.0.1
---
pub_date: 2021-11-15
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-11.0
---
categories: applications
---
summary: Tor Browser 11.0.1 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0.1 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0.1/">distribution directory</a>.</p>
<p>This version provides important bug fixes on Windows, MacOS, and Linux, and includes blockchain explorer <a href="https://blockchair.com/">Blockchair</a> as a search option.</p>
<h2>Blockchair available as search option</h2>
<p><img alt="Blockchair available as a search option in Tor Browser 11." src="/static/images/blog/inline-images/tb-11-blockchair_0.jpg" /></p>
<p>Tor Browser users can now explore the data from 17 different blockchains from a single search engine with Blockchair. Type Blockchair and hit tab or select the Search with Blockchair shortcut to search directly from the address bar. Or, if you prefer, you can also set Blockchair as your Default Search Engine from within Tor Browser’s Search settings (about:preferences#search).</p>
<h2>Full changelog</h2>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0"> Tor Browser 11.0</a> is:</p>
<ul>
<li>Windows, MacOS &amp; Linux:
<ul>
<li>Tor Launcher 0.2.32</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40059">Bug 40059</a>: YEC activist sign empty in about:tor on RTL locales</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40383">Bug 40383</a>: Workaround issue in https-e wasm</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40438">Bug 40438</a>: Add Blockchair as a search engine</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40689">Bug 40689</a>: Change Blockchair Search provider’s HTTP method</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40690">Bug 40690</a>: Browser chrome breaks when private browsing mode is turned off</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40700">Bug 40700</a>: Switch Firefox recommendations off by default</li>
</ul>
</li>
</ul>
<h2>Known issues</h2>
<p>Tor Browser 11.0.1 comes with a number of known issues (please check the following list before submitting a new bug report):</p>
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40668">Bug 40668 </a>: DocumentFreezer &amp; file scheme</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40382">Bug 40382 </a>: Fonts don’t render</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40679">Bug 40679 </a>: Missing features on first-time launch in esr91 on MacOS</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40667">Bug 40667</a>: AV1 videos shows as corrupt files in Windows 8.1</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40666">Bug 40666 </a>: Switching svg.disable affects NoScript settings</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40693">Bug 40693 </a>: Potential Wayland dependency</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40692">Bug 40692</a>: Picture-in-Picture is enabled on tbb 11.0a10</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40705">Bug 40705</a>: “visit our website” link on about:tbupdate pointing to different locations</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40706">Bug 40706</a>: Fix issue in https-e wasm</li>
</ul>
<h2>Join the discussion</h2>
<p>Create an account and join the discussion on the Tor Project's brand new forum:</p>
<p>☞ <a href="https://forum.torproject.net/t/new-release-tor-browser-11-0-1/656">Tor Browser 11.0.1 discussion thread</a></p>

