title: New Release: Tails 5.2
---
pub_date: 2022-07-12
---
author: tails
---
categories:

partners
releases
---
summary:

This release fixes security vulnerabilities in *Tor Browser*.
---
body:

# Changes and updates

## Included software

  * Update _Tor Browser_ to [11.5](https://blog.torproject.org/new-release-tor-browser-115a13/).

  * Update _Thunderbird_ to [91.11.0](https://www.thunderbird.net/en-US/thunderbird/91.11.0/releasenotes/).

# Fixed problems

For more details, read our
[changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

# Known issues

None specific to this release.

See the list of [long-standing
issues](https://tails.boum.org/support/known_issues/).

# Get Tails 5.2

## To upgrade your Tails USB stick and keep your persistent storage

  * Automatic upgrades are available from Tails 5.0 or later to 5.2.

You can [reduce the size of the
download](https://tails.boum.org/doc/upgrade/#reduce) of future automatic
upgrades by doing a manual upgrade to the latest version.

  * If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a [manual upgrade](https://tails.boum.org/doc/upgrade/#manual).

## To install Tails on a new USB stick

Follow our installation instructions:

  * [Install from Windows](https://tails.boum.org/install/windows/)
  * [Install from macOS](https://tails.boum.org/install/mac/)
  * [Install from Linux](https://tails.boum.org/install/linux/)
  * [Install from Debian or Ubuntu using the command line and GnuPG](https://tails.boum.org/install/expert/)

The Persistent Storage on the USB stick will be lost if you install instead of
upgrading.

## To download only

If you don't need installation or upgrade instructions, you can download Tails
5.2 directly:

  * [For USB sticks (USB image)](https://tails.boum.org/install/download/)
  * [For DVDs and virtual machines (ISO image)](https://tails.boum.org/install/download-iso/)

# What's coming up?

Tails 5.3 is [scheduled](https://tails.boum.org/contribute/calendar/) for July
26.

Have a look at our [roadmap](https://tails.boum.org/contribute/roadmap) to see
where we are heading to.

# Support and feedback

For support and feedback, visit the [Support
section](https://tails.boum.org/support/) on the Tails website.
