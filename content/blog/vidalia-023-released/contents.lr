title: Vidalia 0.2.3 released
---
pub_date: 2009-09-03
---
author: phobos
---
tags:

vidalia bundle
drag and drop OS X install
msi installer
updated packages
---
categories:

applications
releases
---
_html_body:

<p>On August 27th, we released Vidalia 0.2.3.  This fixes some more bugs with "Who has used by bridge" functionality and switches to Qt signals for event handling.</p>

<p>The updated Vidalia packages can be found at <a href="https://www.torproject.org/vidalia" rel="nofollow">https://www.torproject.org/vidalia</a></p>

<p>The changes are:</p>

<ul>
<li>Create the data directory before trying to copy over the default<br />
    Vidalia configuration file from inside the application bundle on Mac<br />
    OS X. Affects only OS X drag-and-drop installer users without a<br />
    previous Vidalia installation.</li>
<li>Change all Tor event handling to use Qt's signals and slots mechanism<br />
    instead of custom QEvent subclasses.</li>
<li>Fix another bug that resulted in the "Who has used my bridge?" link<br />
    initially being visible when the user clicks "Setup Relaying" from<br />
    the control panel if they are running a non-bridge relay.<br />
    (Ticket #509, reported by "vrapp")</li>
<li>Always hide the "Who has used my bridge?" link when Tor isn't running,<br />
    since clicking it won't return useful information until Tor actually<br />
    is running.</li>
</ul>

---
_comments:

<a id="comment-2453"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2453" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Biment (not verified)</span> said:</p>
      <p class="date-time">September 06, 2009</p>
    </div>
    <a href="#comment-2453">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2453" class="permalink" rel="bookmark">Any specific reasons why</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any specific reasons why QEvent subclasses are replaced with Qt's signals and slot?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2456"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2456" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 07, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2453" class="permalink" rel="bookmark">Any specific reasons why</a> by <span>Biment (not verified)</span></p>
    <a href="#comment-2456">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2456" class="permalink" rel="bookmark">The change resulted in much</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The change resulted in much cleaner, simpler and shorter code that made implementing support for all the various status events displayed in 0.2.4 much easier. Informally, it also seems to have made the message log more responsive when logging at Tor's hyper-verbose debug level.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
