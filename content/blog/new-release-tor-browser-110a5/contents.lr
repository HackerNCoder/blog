title: New Release: Tor Browser 11.0a5
---
pub_date: 2021-08-24
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-11.0
---
categories: applications
---
summary: Tor Browser 11.0a5 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a5 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a5/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest <a href="https://blog.torproject.org/new-release-tor-browser-1055">stable release</a> instead.</p>
<p>This version updates Tor to <a href="https://blog.torproject.org/node/2062">0.4.6.7</a> that includes a fix for a security issue. On Android, this version updates Firefox to 91.2.0.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 11.0a4:</a></p>
<ul>
<li>All Platforms
<ul>
<li>Update Tor to 0.4.6.7</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40582">Bug 40582</a>: Tor Browser 10.5.2 tabs always crash on Fedora Xfce Rawhide</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Fenix to 91.2.0</li>
</ul>
</li>
</ul>

