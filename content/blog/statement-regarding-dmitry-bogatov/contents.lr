title: Statement regarding Dmitry Bogatov
---
pub_date: 2017-04-13
---
author: jgay
---
_html_body:

<p>The Tor Project has been following with interest the case of Dmitry Bogatov in Russia, but we have no insight or information other than what we've been reading on publicly available websites. </p>

<p>The Tor Project does not collect any information or data that can be used to identify users of the Tor network. We do collect and publish information about Tor exit nodes and relays when relay operators voluntarily choose to send such information to the Tor Project servers. Data about individual Tor relays and the Tor network can be explored through the following sites: </p>

<ul>
<li><a href="https://metrics.torproject.org/" rel="nofollow">Metrics Portal</a> provides analytics for the Tor network, including graphs of its available bandwidth and estimated userbase; </li>
<li><a href="https://atlas.torproject.org/" rel="nofollow">Atlas</a> is a web application to learn about currently running Tor relays; and </li>
<li><a href="https://exonerator.torproject.org/" rel="nofollow">ExoneraTor</a> answers the question of whether there was a Tor relay running on a given IP address on a given date.
</li>
</ul>

<p>What we know right now is that serious accusations of wrongdoing have been made against a valued member of our community, a person who has, among other things, been a Tor relay operator, Debian Developer, GNU developer, and privacy activist. We are collecting facts, monitoring the situation closely, and sharing information with allied organizations and individuals.</p>

