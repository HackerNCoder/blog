title: New Feature: Tor Interpreter
---
pub_date: 2014-05-26
---
author: atagar
---
tags: stem
---
_html_body:

<p>Hi all, after a couple months down in the engine room I'm delighted to announce a new Stem feature for advanced Tor users and developers!</p>

<p><b><a href="https://stem.torproject.org/tutorials/down_the_rabbit_hole.html" rel="nofollow">Stem's Interpreter Tutoral</a></b></p>

<p>The control interpreter is a new method for interacting with Tor's control interface that combines an interactive python interpreter with raw access similar to telnet. This adds several usability features, such as...</p>

<ul>
<li>Irc-style commands like '/help'.</li>
<li>Tab completion for Tor's controller commands.</li>
<li>History scrollback by pressing up/down.</li>
<li>Transparently handles Tor authentication at startup.</li>
<li>Colorized output for improved readability.</li>
</ul>

<p>This is the last major feature going into the Stem's 1.2.0 release, which is coming out later this month. Until then you can easily give it a whirl with...</p>

<p>% git clone <a href="https://git.torproject.org/stem.git" rel="nofollow">https://git.torproject.org/stem.git</a><br />
% cd stem<br />
% ./tor-prompt</p>

<p>Running into an issue? Got a feature request? As always feedback appreciated! -Damian</p>

---
_comments:

<a id="comment-61787"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61787" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 26, 2014</p>
    </div>
    <a href="#comment-61787">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61787" class="permalink" rel="bookmark">Neat! Nicely done.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Neat! Nicely done.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-61815"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61815" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 26, 2014</p>
    </div>
    <a href="#comment-61815">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61815" class="permalink" rel="bookmark">We have some questions for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have some questions for you Damian:</p>
<p>1. Are you working for the NSA?</p>
<p>2. Have you ever been approached by the NSA to insert backdoors in your software? (You can give a verbal "yes" with a shaking of the head to signify "no")</p>
<p>3. Are there backdoors in the Stem's interpreter for the NSA to make use of?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-61927"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61927" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-61815" class="permalink" rel="bookmark">We have some questions for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-61927">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61927" class="permalink" rel="bookmark">[[Edit: I  spent a while</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[[Edit: I  spent a while trying to figure out if I should moderate this comment from an anonymous user or what -- but I decided to let it go through intact. Note that the real Damian answers below, and this is some person trolling you. But hey, make up your own mind. :) --Roger]]</p>
<p>&gt;1. Are you working for the NSA?<br />
Yes.</p>
<p>&gt;2. Have you ever been approached by the NSA to insert backdoors in your software? ?(You can give a verbal "yes" with a shaking of the head to signify "no")<br />
Yes and we have.</p>
<p>&gt;3. Are there backdoors in the Stem's interpreter for the NSA to make use of?<br />
Of course.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62433"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62433" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-61927" class="permalink" rel="bookmark">[[Edit: I  spent a while</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62433">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62433" class="permalink" rel="bookmark">Yes ! We Can !</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes ! We Can !</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-61820"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61820" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  atagar
  </article>
    <div class="comment-header">
      <p class="comment__submitted">atagar said:</p>
      <p class="date-time">May 26, 2014</p>
    </div>
    <a href="#comment-61820">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61820" class="permalink" rel="bookmark">For all three questions the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For all three questions the answer is nope. On a side note the interpreter is a pretty small codebase so it would be easy to audit if you're really that concerned.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-61966"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61966" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to atagar</p>
    <a href="#comment-61966">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61966" class="permalink" rel="bookmark">thank you for this post</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thank you for this post</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-61833"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61833" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 26, 2014</p>
    </div>
    <a href="#comment-61833">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61833" class="permalink" rel="bookmark">A lot of time the my TBB is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A lot of time the my TBB is too slowly than it should be, here is a trick.</p>
<p>First you get the Vidalia standalone (me downloads i686), then you can see the connection and the status in Tor Network Map. If you feel the network slowly than it should be, delete the related connection (maybe named node or relay) from the Tor Network Map. Now you can see some nodes status is new, if 1 second later it is still new, delete it. If you see the nodes status is building...The new node will build and may help you visited the internet. If not, keep deleting and refreshing your Firefox ( yes, some times deleting the node will make your browser stop connecting to internet) until you the traffic in Tor Bandwidth Usage is not zero.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-62130"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62130" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2014</p>
    </div>
    <a href="#comment-62130">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62130" class="permalink" rel="bookmark">Can SOMEONE from the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can SOMEONE from the torproject PLEASE REMOVE Startpage.com and Ixquick.com as the STANDARD Search Engine??? </p>
<p>They LOG STUFF ABOUT YOU! So the OTHER DAY Im using TOR with a GERMAN IP. Just Searched for "DEPRESSION" on IXQUICK. Guess what result I got first? <a href="http://www.depression-grind.de" rel="nofollow">www.depression-grind.de</a> (NON-sponsored-site) - A GERMAN WEBSITE. I CHANGED the IP addres (New IDENTITY Button). Search for DEPRESSION again. FIRST RESULT is COMPLETELY DIFFERENT!!!!!!!</p>
<p>THEY...LOG...YOUR...COUNTRY...AND...PROBABLY...IP...TOO</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-63043"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63043" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 12, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62130" class="permalink" rel="bookmark">Can SOMEONE from the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-63043">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63043" class="permalink" rel="bookmark">If you&#039;re using Tor, they</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you're using Tor, they don't get your country or your IP address. That's one of the many goals of Tor.</p>
<p>More broadly though, we're running pretty low on search engines that don't suck and that don't make it really tough to interact with them over Tor. Maybe we've run out entirely, in fact, if we want to achieve both of those properties.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
