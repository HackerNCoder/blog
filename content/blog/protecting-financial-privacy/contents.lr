title: Protecting Financial Privacy
---
pub_date: 2019-12-18
---
author: steph
---
tags:

cryptocurrency
take back the internet
---
_html_body:

<p><em>This guest post is from Alex Gladstein, Chief Strategy Officer for Human Rights Foundation. To read even more about Tor and cryptocurrency see <a href="https://blog.torproject.org/tor-heart-cryptocurrencies">this post</a>. We are nearing the end of our end-of-year campaign #TakeBacktheInternet. Please donate <a href="https://donate.torproject.org/cryptocurrency">cryptocurrency</a> or <a href="https://torproject.org/donate/donate-tbi-bp9">fiat</a> today.</em></p>
<p><em>Disclaimer: we invite guest posts on this blog based on the content of the post, and these posts aren't intended to endorse the guest poster or their organization.</em></p>
<hr />
<p><img alt="Alex Gladstein " hspace="20" src="/static/images/blog/inline-images/alex-gladstein.jpeg" vspace="5" width="175" class="align-right" />The Tor community understands the paramount importance of private communications. While politicians or pundits may try to discredit encrypted messaging by saying, “what do you have to hide?” or by asserting that only drug dealers or criminals would need to conceal their communications, you know better. Without privacy, we’re on a slippery slope to an Orwellian surveillance state, similar to what’s tragically unfolding inside China today.</p>
<p>But what you may not have considered is the equally paramount importance of financial privacy. The money we all use on a daily basis has evolved from a bearer asset to a surveillance mechanism. Your transaction history arguably says more about you than your email and text history. If I really want to know what you’re doing, I want to see your bank and credit card statements. And with the latest big-data analysis, a government can sift through bulk financial data to enforce a police state or install ambitious social engineering schemes. Similarly, corporations can continue to exploit and sell your personal information in a growing wave of surveillance capitalism.</p>
<p>In the same way that we rely on technology like Tor to protect our communications and internet browsing, we need to start protecting our financial privacy. This wasn’t possible online until the invention of Bitcoin, a peer-to-peer payment network with no middleman. In Bitcoin, there is no central authority that approves transactions. Instead, it’s done through a decentralized global competition, so no censorship is possible, even from state actors. And there is no third party freezing undesirable payments or controlling detailed user data.</p>
<p>It’s critical to note that Bitcoin is a pseudonymous network, and with enough effort, your transactions can be linked to real-world identities and users can be deanonymized. But it’s equally critical to note that with good operational security, you can make it difficult for anyone to know how you are spending your money. The caveat is that this is essentially an expert-level task at the moment. Strategies like running a full Bitcoin node over Tor, using cutting-edge mixers, and avoiding centralized exchanges that enforce KYC “Know Your Customer” regulations are out of reach for the average Bitcoin user. But the blueprint exists for making daily payments for the average person private using Bitcoin as a foundational technology.</p>
<p>One way to do this may be through the Lightning Network, an open-source payment network that sits on Bitcoin as a second layer, harnessing onion routing to help transmit Bitcoin in a way that’s extremely difficult to trace. Just as paper notes were used as a scaling solution for gold bars, and Visa was used a scaling solution for the U.S. dollar, Lightning could very well be the scaling solution for Bitcoin, with the extra benefit that it can transform Bitcoin’s pseudonymous payment structure into something that’s virtually anonymous. Lightning is nascent today, and needs a lot of work. But the building blocks are there for you to be able to, within a year or two, use it to make the equivalent of cash transactions in the digital world.</p>
<p>When you buy a cup of coffee, or take friends to dinner, or subscribe to a podcast, or purchase a book, no one else needs to know. That is your private decision. And you can still protect that decision by using cash to do those things. But increasingly, cash is fading around the world. In a decade or two, it will be entirely gone. Using digital cash is one way to take back the internet and protect what privacy we have. The Tor and Bitcoin communities can make for powerful allies in this effort.</p>
<hr />
<p><em>Tor software is critical to the future of privacy online. Help us keep Tor strong and take back the internet by making a <a href="https://donate.torproject.org/cryptocurrency">cryptocurrency</a> or <a href="https://torproject.org/donate/donate-tbi-bp9">fiat donation</a> today.</em></p>

