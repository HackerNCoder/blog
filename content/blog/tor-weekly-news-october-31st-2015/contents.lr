title: Tor Weekly News — October 31st, 2015
---
pub_date: 2015-10-31
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-seventh issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>IETF reserves .onion as a Special-Use Domain Name</h1>

<p>Several years of effort by Tor Project members and contributors bore fruit this week when the Internet Engineering Task Force, which develops and promotes voluntary standards for Internet technologies, <a href="https://blog.torproject.org/blog/landmark-hidden-services-onion-names-reserved-ietf" rel="nofollow">recognized the .onion suffix as a special-use domain name</a>.</p>

<p>As Jacob Appelbaum, who led the charge along with Facebook security engineer Alec Muffett, explained: “IETF name reservations are part of a lesser known process that ensures a registered Special-Use Domain Name will not become a Top Level Domain (TLD) to be sold by the Internet Corporation For Assigned Names and Numbers (ICANN).” In other words, it will not be possible for domain registrars to sell web addresses ending in .onion; if it were, it would create problems for Tor’s hidden service system, which uses that suffix to allow users to run anonymous and censorship-resistant web services accessible via the Tor Browser.</p>

<p>Another benefit of the name reservation is that it will now be possible to buy Extended Validation (EV) SSL certificates for .onion domains, a system which Facebook has trialled <a href="https://blog.torproject.org/blog/facebook-hidden-services-and-https-certs" rel="nofollow">on its own popular hidden service</a>.</p>

<p>“We think that this is a small and important landmark in the movement to build privacy into the structure of the Internet”, wrote Jacob. Congratulations to all those who spent time drafting this proposal and advocating for its adoption.</p>

<h1>Tor proposal updates</h1>

<p>Tor’s body of development proposals, documents that plan for improvements and changes in Tor’s software ecosystem, has seen some additions, updates, and reviews over the past week.</p>

<p>Nick Mathewson published <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009798.html" rel="nofollow">proposal 256</a>, which examines methods for revoking the long-lived public keys used by Tor relays and directory authorities in the event that they are compromised, or the operator believes there is a significant possibility that they have been compromised. Andrea Shepard wrote <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009821.html" rel="nofollow">proposal 258</a>, explaining how directory authorities could mitigate the risk of denial-of-service (DOS) attacks by classifying the types of directory requests they receive and setting thresholds for each. Nick and Andrea together published <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009799.html" rel="nofollow">proposal 257</a>, which identifies the different functions performed by directory authorities and examines how the risk of DOS attacks could be reduced by “isolating the security-critical, high-resource, and availability-critical pieces of our directory infrastructure from one another”.</p>

<p>George Kadianakis published <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009762.html" rel="nofollow">a review</a> of all the open proposals relevant to next-generation hidden services, giving a summary of each one along with its current status, “so that researchers and developers have easier access to them”.</p>

<p>Proposal 250, which specifies how directory authorities can come up with a shared random value every day, and which George describes as “a prerequisite” for all other work on next-gen hidden services, was itself updated to reflect changes in the implementation, which is almost finished, as David Goulet <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009812.html" rel="nofollow">explained</a>. Finally, Tim Wilson-Brown (teor) published a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009763.html" rel="nofollow">revised version</a> of the as-yet unnumbered proposal for “rendevous single onion services”, “an alternative design for single onion services, which trade service-side location privacy for improved performance, reliability, and scalability”.</p>

<p>If you have any comments on these or other Tor proposals, feel free to post your thoughts to the tor-dev mailing list.</p>

<h1>Miscellaneous news</h1>

<p>The <a href="https://torbsd.github.io/" rel="nofollow">Tor BSD Diversity Project</a>, “an effort to extend the use of the BSD Unixes into the Tor ecosystem, from the desktop to the network”, <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009800.html" rel="nofollow">announced</a> the release of an OpenBSD port of Tor Browser 5.0.3, its sixth Tor Browser release for BSD systems. See attila’s announcement for download instructions, as well as a report on the TDP’s other development and advocacy activities.</p>

<p>Tor’s Metrics team, “a group of Tor people who care about measuring and analyzing things in the public Tor network”, now has its own public mailing list and wiki page, as Karsten Loesing <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009808.html" rel="nofollow">announced</a>. There is a simple step to complete before you can post freely to the list, but anyone interested in “measurements and analysis” is welcome to listen in on discussions, and to check the team’s roadmap and workflow on the wiki page.</p>

<p>“In an attempt to make Pluggable Transports more accessible to other people, and to have a spec that is more applicable and useful to other projects that seek to use Pluggable Transports for circumvention”, Yawning Angel <a href="https://lists.torproject.org/pipermail/tor-dev/2015-October/009815.html" rel="nofollow">drafted a rewrite</a> of the pluggable transports spec document. No behavior changes are specified in this rewrite, but “unless people have serious objections, this will replace the existing PT spec, to serve as a stop-gap while the next revision of the PT spec (that does alter behavior) is being drafted/implemented”.</p>

<p>Simone Bassano published a <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-October/000353.html" rel="nofollow">report</a> on the OONI hackathon that took place in Rome at the start of October. A working beta version of MeasurementKit and progress on NetworkMeter, as well as ways to make use of censorship data, were among the outcomes.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

