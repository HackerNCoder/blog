title: Tor 0.2.6.4-rc is released
---
pub_date: 2015-03-10
---
author: nickm
---
tags:

tor
release candidate
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.6.4-rc fixes an issue in the directory code that an attacker might be able to use in order to crash certain Tor directories. It also resolves some minor issues left over from, or introduced in, Tor 0.2.6.3-alpha or earlier. </p>

<p>If no serious issues are found in this release, the next 0.2.6 release will make 0.2.6 the officially stable branch of Tor.</p>

<p>You can download the source from the usual place on the website. Packages should be up in a few days.</p>

<p><b>NOTE:</b> This is an alpha release. Please expect bugs.</p>

<h2>Changes in version 0.2.6.4-rc - 2015-03-09</h2>

<ul>
<li>Major bugfixes (crash, OSX, security):
<ul>
<li>Fix a remote denial-of-service opportunity caused by a bug in OSX's _strlcat_chk() function. Fixes bug 15205; bug first appeared in OSX 10.9.
  </li>
</ul>
</li>
<li>Major bugfixes (relay, stability, possible security):
<ul>
<li>Fix a bug that could lead to a relay crashing with an assertion failure if a buffer of exactly the wrong layout is passed to buf_pullup() at exactly the wrong time. Fixes bug 15083; bugfix on 0.2.0.10-alpha. Patch from "cypherpunks".
  </li>
<li>Do not assert if the 'data' pointer on a buffer is advanced to the very end of the buffer; log a BUG message instead. Only assert if it is past that point. Fixes bug 15083; bugfix on 0.2.0.10-alpha.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major bugfixes (FreeBSD IPFW transparent proxy):
<ul>
<li>Fix address detection with FreeBSD transparent proxies, when "TransProxyType ipfw" is in use. Fixes bug 15064; bugfix on 0.2.5.4-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (Linux seccomp2 sandbox):
<ul>
<li>Pass IPPROTO_TCP rather than 0 to socket(), so that the Linux seccomp2 sandbox doesn't fail. Fixes bug 14989; bugfix on 0.2.6.3-alpha.
  </li>
<li>Allow AF_UNIX hidden services to be used with the seccomp2 sandbox. Fixes bug 15003; bugfix on 0.2.6.3-alpha.
  </li>
<li>Upon receiving sighup with the seccomp2 sandbox enabled, do not crash during attempts to call wait4. Fixes bug 15088; bugfix on 0.2.5.1-alpha. Patch from "sanic".
  </li>
</ul>
</li>
<li>Minor features (controller):
<ul>
<li>Messages about problems in the bootstrap process now include information about the server we were trying to connect to when we noticed the problem. Closes ticket 15006.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip to the March 3 2015 Maxmind GeoLite2 Country database.
  </li>
<li>Update geoip6 to the March 3 2015 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (logs):
<ul>
<li>Quiet some log messages in the heartbeat and at startup. Closes ticket 14950.
  </li>
</ul>
</li>
<li>Minor bugfixes (certificate handling):
<ul>
<li>If an authority operator accidentally makes a signing certificate with a future publication time, do not discard its real signing certificates. Fixes bug 11457; bugfix on 0.2.0.3-alpha.
  </li>
<li>Remove any old authority certificates that have been superseded for at least two days. Previously, we would keep superseded certificates until they expired, if they were published close in time to the certificate that superseded them. Fixes bug 11454; bugfix on 0.2.1.8-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a compilation warning on s390. Fixes bug 14988; bugfix on 0.2.5.2-alpha.
  </li>
<li>Fix a compilation warning on FreeBSD. Fixes bug 15151; bugfix on 0.2.6.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Fix endianness issues in unit test for resolve_my_address() to have it pass on big endian systems. Fixes bug 14980; bugfix on Tor 0.2.6.3-alpha.
  </li>
<li>Avoid a side-effect in a tor_assert() in the unit tests. Fixes bug 15188; bugfix on 0.1.2.3-alpha. Patch from Tom van der Woerdt.
  </li>
<li>When running the new 'make test-stem' target, use the configured python binary. Fixes bug 15037; bugfix on 0.2.6.3-alpha. Patch from "cypherpunks".
  </li>
<li>When running the zero-length-keys tests, do not use the default torrc file. Fixes bug 15033; bugfix on 0.2.6.3-alpha. Reported by "reezer".
  </li>
</ul>
</li>
<li>Directory authority IP change:
<ul>
<li>The directory authority Faravahar has a new IP address. This closes ticket 14487.
  </li>
</ul>
</li>
<li>Removed code:
<ul>
<li>Remove some lingering dead code that once supported mempools. Mempools were disabled by default in 0.2.5, and removed entirely in 0.2.6.3-alpha. Closes more of ticket 14848; patch by "cypherpunks".
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-89610"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89610" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2015</p>
    </div>
    <a href="#comment-89610">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89610" class="permalink" rel="bookmark">Excelente.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Excelente.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89652"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89652" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 11, 2015</p>
    </div>
    <a href="#comment-89652">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89652" class="permalink" rel="bookmark">please Add Tor button  and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>please Add Tor button  and (New Tor circuit For this site) option in next version of Tor browser(4.0.5?)  ,it's so useful</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89681"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89681" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 11, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89652" class="permalink" rel="bookmark">please Add Tor button  and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89681">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89681" class="permalink" rel="bookmark">From what I understand, both</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From what I understand, both features are landing in the 4.5 branch.  The 4.0 branch is on maintenance only at this point and isn't adding new features.  Also, this is a posting about the Tor routing software itself, not Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89693"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89693" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 11, 2015</p>
    </div>
    <a href="#comment-89693">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89693" class="permalink" rel="bookmark">Shouldnt tor start using rsa</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Shouldnt tor start using rsa 2048 for onion name generation?  1024 is deprecated and i dont feel safe.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89714"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89714" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 11, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89693" class="permalink" rel="bookmark">Shouldnt tor start using rsa</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89714">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89714" class="permalink" rel="bookmark">You will like proposal</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You will like proposal 224:<br />
<a href="https://gitweb.torproject.org/torspec.git/tree/proposals/224-rend-spec-ng.txt" rel="nofollow">https://gitweb.torproject.org/torspec.git/tree/proposals/224-rend-spec-…</a></p>
<p>Step one is to continue getting feedback from the research community on whether this design has any issues that will surprise us later. Then step two is to actually implement it. Help appreciated on both parts!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89826"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89826" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2015</p>
    </div>
    <a href="#comment-89826">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89826" class="permalink" rel="bookmark">Latest version of TorBrowser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Latest version of TorBrowser (4.0.5) is connecting to (some) websites without using proxy.</p>
<p><a href="http://i.imgur.com/ho8U7kc.png" rel="nofollow">http://i.imgur.com/ho8U7kc.png</a></p>
<p>As seen in this picture. Tor Browser is configured to go through 127.0.0..1:9150 so it should not show in Proxifier unless it ignores its proxy settings. It showed up connecting to random sites I am not even browsing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89848"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89848" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 14, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89826" class="permalink" rel="bookmark">Latest version of TorBrowser</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89848">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89848" class="permalink" rel="bookmark">There is no Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is no Tor Browser 4.0.5 yet. Are you sure you have an actual Tor Browser?</p>
<p>That said, a blog post about the latest Tor release is unlikely to be the right place to get help for this topic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89854"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89854" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-89854">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89854" class="permalink" rel="bookmark">I am sorry it was 4.0.4.
Yes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am sorry it was 4.0.4.</p>
<p>Yes I am completely sure. I downloaded it from this website. I think if I am capable of setting up a torified VM then I probably can download Tor Browser from the correct website.</p>
<p>You should also experiment with the proxy settings. When you enter invalid SOCKS settings(for connecting to Tor network) it then doesn't connect but when you enter correct ones (without restarting Tor Browser(or Launcher???)) it still won't connect unless you restart.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-89882"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89882" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2015</p>
    </div>
    <a href="#comment-89882">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89882" class="permalink" rel="bookmark">Why is not there a hidden</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is not there a hidden service for Torproject.org? Torproject should promote their creations, many don't even know what hidden services are.</p>
</div>
  </div>
</article>
<!-- Comment END -->
