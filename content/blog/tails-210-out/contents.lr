title: Tails 2.10 is out
---
pub_date: 2017-01-24
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_2.9.1/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h2>New features</h2>

<ul>
<li>
We installed <a href="https://tails.boum.org/doc/anonymous_internet/onionshare/" rel="nofollow"><em>OnionShare</em></a>, a tool for anonymous file sharing.
</li>
<li>
We enabled the <a href="https://tails.boum.org/doc/anonymous_internet/Tor_Browser/#circuit_view" rel="nofollow">circuit view</a> in <em>Tor Browser</em>.
</li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li>
Upgrade <em>Tor</em> to 0.2.9.9.
</li>
<li>
Upgrade <em>Tor Browser</em> to 6.5.
</li>
<li>
Upgrade <em>Linux</em> to 4.8. This should improve the support for newer hardware (graphics, Wi-Fi, etc.)
</li>
<li>
Upgrade <em>Icedove</em> to 45.6.0.
</li>
<li>
Replace <em>AdBlock Plus</em> with <em>uBlock Origin</em>.
</li>
<li>
Configure the <em>APT</em> package manage to use Debian's Onion services.
</li>
<li>
Install the <em>AMDGPU</em> display driver. This should improve the support for newer AMD graphics adapters.
</li>
<li>
Renamed the <em>Boot Loader Menu</em> entries from "Live" to "Tails", and replaced the confusing "failsafe" wording with "Troubleshooting Mode".
</li>
<li>
Add support for <a href="https://en.wikipedia.org/wiki/exFAT" rel="nofollow">exFAT</a>.
</li>
<li>
Remove <em>Nyx</em> (previously called <em>arm</em>).
</li>
<li>
Rewrite <em>Tor control port filter</em> entirely. Now Tails can safely support <em>OnionShare</em>, the circuit view of <em>Tor Browser</em>, and similar. This also enabled Whonix to replace their own similar piece of software with this one.
</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>
Made <em>OnionCircuits</em> compatible with the <em>Orca</em> screen reader.
</li>
</ul>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.</p>

<h2>Known issues</h2>

<p>None specific to this release.</p>

<p>See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h2>Get Tails 2.10</h2>

<ul>
<li>
To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.
</li>
<li>
To upgrade, an automatic upgrade is available from 2.7 and 2.9.1 to 2.10.<br />
If you cannot do an automatic upgrade or if you fail to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.
</li>
<li>
<a href="https://tails.boum.org/install/download/" rel="nofollow">Download Tails 2.10.</a>
</li>
</ul>

<h2>What's coming up?</h2>

<p>Tails 2.11 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for March 3rd.</p>

<p>Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate/#2.10" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

