title: New Release: Tor Browser 11.0a6 (Android Only)
---
pub_date: 2021-09-02
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-11.0
---
categories: applications
---
summary: Tor Browser 11.0a6 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a6 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a6/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-1055">latest stable release</a> instead.</p>
<p>This version updates Fenix's Geckoview component to 92.0b9.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 11.0a5:</a></p>
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40611">Bug 40611</a>: Rebase geckoview patches onto 92.0b9</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292725"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292725" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon (not verified)</span> said:</p>
      <p class="date-time">September 07, 2021</p>
    </div>
    <a href="#comment-292725">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292725" class="permalink" rel="bookmark">Updated from 11.0a5 and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Updated from 11.0a5 and Safest Mode does not disable Javascript. Known issue or local config glitch?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292814"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292814" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292725" class="permalink" rel="bookmark">Updated from 11.0a5 and…</a> by <span>Anon (not verified)</span></p>
    <a href="#comment-292814">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292814" class="permalink" rel="bookmark">Safest mode is not supposed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Safest mode is not supposed to disable <span class="geshifilter"><code class="php geshifilter-php">javascript<span style="color: #339933;">.</span>enabled</code></span> in <span class="geshifilter"><code class="php geshifilter-php">about<span style="color: #339933;">:</span>config</code></span>. Safest mode is supposed to disable JavaScript in the default options of NoScript. When you look in the correct places, are the options set as they are supposed to be? Did you reconfigure those options by not using the security level button?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292823"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292823" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ba=in (not verified)</span> said:</p>
      <p class="date-time">September 23, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292725" class="permalink" rel="bookmark">Updated from 11.0a5 and…</a> by <span>Anon (not verified)</span></p>
    <a href="#comment-292823">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292823" class="permalink" rel="bookmark">Iam facing this from last…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Iam facing this from last two updates. It's an issue.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292770"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292770" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>JustAnotherUser (not verified)</span> said:</p>
      <p class="date-time">September 14, 2021</p>
    </div>
    <a href="#comment-292770">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292770" class="permalink" rel="bookmark">JavaScript seems to be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>JavaScript seems to be disabled regardless of settings. Bug? Feature?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292813"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292813" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon (not verified)</span> said:</p>
      <p class="date-time">September 21, 2021</p>
    </div>
    <a href="#comment-292813">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292813" class="permalink" rel="bookmark">The downloads tab doesn&#039;t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The downloads tab doesn't work, it shows no history whilst a file is downloading or when it's finished.<br />
If you start a download and close the tab you will no longer see the file downloading in the notification tray and you can't see it through the downloads tab either. This is an easily recreated glitch.</p>
<p>1) Open the app, using safest mode as everyone ought to<br />
2) Go to obe.net (they help Tor)<br />
3) Select speed test<br />
4) Choose a server<br />
5) Tap on the 100MB test (this downloads a 100MB archive file of scrambled useless data)<br />
6) Whilst it is downloading, exit the tab for the speedtest, this will cause the progress animation in the notification tray to disappear<br />
7) In the app tap the three dots for options, tap downloads, there will be nothing<br />
8) Look at Tor on the notification tray, it still shows data passing and will do so until the download completes (or more likely, times out half way through)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292826"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292826" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Catfelix (not verified)</span> said:</p>
      <p class="date-time">September 23, 2021</p>
    </div>
    <a href="#comment-292826">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292826" class="permalink" rel="bookmark">Developer, hello! The Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Developer, hello! The Tor browser for Android alpha since version 11. 0a5-11.0a6-11.0a7 javascript does not work on websites.<br />
Default browser settings.<br />
Nothing happens when you change the security settings. Javascript is still not working.</p>
</div>
  </div>
</article>
<!-- Comment END -->
