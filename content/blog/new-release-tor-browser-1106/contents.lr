title: New Release: Tor Browser 11.0.6 (Windows, macOS, Linux, Android)
---
pub_date: 2022-02-09
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.0.6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.6 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.6/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-05/) to Firefox.

Tor Browser 11.0.6 updates Firefox on Windows, macOS, and Linux to 91.6.0esr.

We use the opportunity as well to update various other components of Tor Browser: NoScript to 11.2.16. We also switch to the latest Go version (1.16.13) for building our Go-related projects.

The full changelog since [Tor Browser 11.0.4](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows, macOS, & Linux
  - Update Firefox to 91.6.0esr
  - Update NoScript to 11.2.16
  - Update Tor Launcher to 0.2.33
  - [Bug tor-browser#40795](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40795): Revert Deutsche Welle v2 redirect
  - [Bug tor-browser#40679](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40679): Missing features on first-time launch in esr91
  - Added extensions.torlauncher.launch_delay debug pref to simulate slow tor daemon launch
- Build System
  - Windows + OS X + Linux + Android
    - Update Go to 1.16.13
    - [Bug tor-browser-build#40413](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40413): Removed lsb_release from Python build script
    - [Bug tor-browser-build#40416](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40416): Pick up obfsproxy 0.0.12
