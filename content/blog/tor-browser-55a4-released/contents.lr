title: Tor Browser 5.5a4 is released
---
pub_date: 2015-11-04
---
author: gk
---
tags:

tor browser
tbb
tbb-5.5
---
categories:

applications
releases
---
_html_body:

<p>A new alpha Tor Browser release is available for download in the <a href="https://dist.torproject.org/torbrowser/5.5a4/" rel="nofollow">5.5a4 distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha" rel="nofollow">alpha download page</a>.</p>

<p>This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox-esr/#firefoxesr38.4" rel="nofollow">security updates</a> to Firefox.</p>

<p>Moreover, it comes with Tor 0.2.7.4-rc and a number of other improvements. Most notably, we included Yan Zhu's fix for not leaking the Referer header when leaving a .onion domain and finally sorted out the HTTPS-Everywhere build problems allowing us to ship its latest version in our bundles again.</p>

<p>We fixed usability issues caused by our font fingerprinting patches and included a new defense against figerprinting users via available MIME types and plugins. Finally, besides additional minor bug fixes and clean-ups, we included an updated NoScript version.</p>

<p>Here is the complete changelog since 5.5a3:</p>

<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 38.4.0esr
   </li>
<li>Update Tor to 0.2.7.4-rc
   </li>
<li>Update NoScript to 2.6.9.39
   </li>
<li>Update HTTPS-Everywhere to 5.1.1
   </li>
<li>Update Torbutton to 1.9.4.1
<ul>
<li>Bug 9623: Spoof Referer when leaving a .onion domain
       </li>
<li>Bug 16620: Remove old window.name handling code
       </li>
<li>Bug 17164: Don't show text-select cursor on circuit display
       </li>
<li>Bug 17351: Remove unused code
       </li>
<li>Translation updates
     </li>
</ul>
</li>
<li>Bug 17207: Hide MIME types and plugins from websites
   </li>
<li>Bug 16909+17383: Adapt to HTTPS-Everywhere build changes
   </li>
<li>Bug 16620: Move window.name handling into a Firefox patch
   </li>
<li>Bug 17220: Support math symbols in font whitelist
   </li>
<li>Bug 10599+17305: Include updater and build patches needed for hardened builds
   </li>
<li>Bug 17318: Remove dead ScrambleSuit bridge
   </li>
<li>Bug 17428: Remove default Flashproxy bridges
   </li>
<li>Bug 17473: Update meek-amazon fingerprint
 </li>
</ul>
</li>
<li>Windows
<ul>
<li>Bug 17250: Add localized font names to font whitelist
    </li>
</ul>
</li>
<li>OS X
<ul>
<li>Bug 17122: Rename Japanese OS X bundle
   </li>
</ul>
</li>
<li>Linux
<ul>
<li>Bug 17329: Ensure that non-ASCII characters can be typed (fixup of #5926)
   </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-113973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113973" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2015</p>
    </div>
    <a href="#comment-113973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113973" class="permalink" rel="bookmark">Thank you!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-115433"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-115433" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 1969</p>
    </div>
    <a href="#comment-115433">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-115433" class="permalink" rel="bookmark">(No subject)</a></h3>
      <div></div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114000"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114000" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2015</p>
    </div>
    <a href="#comment-114000">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114000" class="permalink" rel="bookmark">Bug 9263 -&gt; 9623</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bug 9263 -&gt; 9623</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-114021"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114021" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-114000" class="permalink" rel="bookmark">Bug 9263 -&gt; 9623</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-114021">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114021" class="permalink" rel="bookmark">Indeed, thanks.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Indeed, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-114085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114085" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 06, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-114085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114085" class="permalink" rel="bookmark">Actually this happens almost</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually this happens almost every release lol.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-114002"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114002" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2015</p>
    </div>
    <a href="#comment-114002">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114002" class="permalink" rel="bookmark">You guys are the best!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You guys are the best!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114006"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114006" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2015</p>
    </div>
    <a href="#comment-114006">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114006" class="permalink" rel="bookmark">| Bug 9263: Spoof Referer</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>| <i>Bug 9263: Spoof Referer when leaving a .onion domain </i></p>
<p><a href="https://trac.torproject.org/projects/tor/ticket/9263" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/9263</a></p>
<p>gives</p>
<p><cite>Traditional /bin/sh is unhappy about some file globbing</cite></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114010"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114010" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2015</p>
    </div>
    <a href="#comment-114010">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114010" class="permalink" rel="bookmark">Oh, thanks a lot !!! Finally</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh, thanks a lot !!! Finally I can type directly in my native language in Tor Browser on Linux without the need to copy-past. That was kind of annoying tho. I appreciate your hard work on providing us such a wonderful web browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-114141"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114141" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 06, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-114010" class="permalink" rel="bookmark">Oh, thanks a lot !!! Finally</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-114141">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114141" class="permalink" rel="bookmark">I can&#039;t do it on linux with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't do it on linux with the drag and drop</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-137222"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-137222" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-114141" class="permalink" rel="bookmark">I can&#039;t do it on linux with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-137222">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-137222" class="permalink" rel="bookmark">What do you mean? And this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What do you mean? And this works in the stable releases (like 5.0.4)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-114012"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114012" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2015</p>
    </div>
    <a href="#comment-114012">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114012" class="permalink" rel="bookmark">Thank you !</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114016"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114016" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114016">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114016" class="permalink" rel="bookmark">Thanks for making this, i</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for making this, i really like things like TOR :) (will make a donation)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114018"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114018" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114018">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114018" class="permalink" rel="bookmark">thanks to you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks to you</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114031"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114031" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114031">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114031" class="permalink" rel="bookmark">i 2nd dat!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i 2nd dat!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114041"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114041" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114041">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114041" class="permalink" rel="bookmark">Thank you! My net safety</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you! My net safety relies on you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114058"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114058" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114058">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114058" class="permalink" rel="bookmark">happiness and good karma</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>happiness and good karma :-)<br />
thanks a lot</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114059"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114059" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114059">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114059" class="permalink" rel="bookmark">Very Good Job! Thank you.:)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very Good Job! Thank you.:)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114081"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114081" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2015</p>
    </div>
    <a href="#comment-114081">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114081" class="permalink" rel="bookmark">You guys are the best!Thanks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You guys are the best!Thanks a lot from China!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114115"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114115" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 06, 2015</p>
    </div>
    <a href="#comment-114115">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114115" class="permalink" rel="bookmark">How i can connect villadia</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How i can connect villadia with this version?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-114142"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114142" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 06, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-114115" class="permalink" rel="bookmark">How i can connect villadia</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-114142">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114142" class="permalink" rel="bookmark">Can I do it with the stable</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can I do it with the stable version?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-114118"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114118" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 06, 2015</p>
    </div>
    <a href="#comment-114118">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114118" class="permalink" rel="bookmark">Excellent update. Thanks a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Excellent update. Thanks a bundle!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114130"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114130" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 06, 2015</p>
    </div>
    <a href="#comment-114130">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114130" class="permalink" rel="bookmark">Awesome stuff!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Awesome stuff!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114149"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114149" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 07, 2015</p>
    </div>
    <a href="#comment-114149">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114149" class="permalink" rel="bookmark">There is a large drop in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is a large drop in flow rates with respect to the previous version ... It is simple comparing to Firefox 42 is flow divided by 10 while the previous version was only divided by 2 !<br />
Does the price of success ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114509"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114509" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 14, 2015</p>
    </div>
    <a href="#comment-114509">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114509" class="permalink" rel="bookmark">tor can be faster</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor can be faster</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-114849"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114849" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 17, 2015</p>
    </div>
    <a href="#comment-114849">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114849" class="permalink" rel="bookmark">how do i get all english</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>how do i get all english</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-137757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-137757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 28, 2015</p>
    </div>
    <a href="#comment-137757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-137757" class="permalink" rel="bookmark">very good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>very good</p>
</div>
  </div>
</article>
<!-- Comment END -->
