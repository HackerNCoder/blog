title: Tor 0.2.2.19-alpha is out
---
pub_date: 2010-11-26
---
author: erinn
---
tags:

tor
openssl
bug fixes
alpha release
openssl fixes
---
categories:

network
releases
---
_html_body:

<p>Yet another OpenSSL security patch broke its compatibility with Tor:<br />
Tor 0.2.2.19-alpha makes relays work with OpenSSL 0.9.8p and 1.0.0.b.</p>

<p><a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a></p>

<p>The original announcement is at <a href="http://archives.seul.org/or/talk/Nov-2010/msg00172.html" rel="nofollow">http://archives.seul.org/or/talk/Nov-2010/msg00172.html</a></p>

<p>Changes in version 0.2.2.19-alpha - 2010-11-21<br />
<strong>Major bugfixes:</strong></p>

<ul>
<li> Resolve an incompatibility with openssl 0.9.8p and openssl 1.0.0b:<br />
      No longer set the tlsext_host_name extension on server SSL objects;<br />
      but continue to set it on client SSL objects. Our goal in setting<br />
      it was to imitate a browser, not a vhosting server. Fixes bug 2204;<br />
      bugfix on 0.2.1.1-alpha.</li>
<p>  <strong> Minor bugfixes:</strong></p>
<li> Try harder not to exceed the maximum length of 50 KB when writing<br />
      statistics to extra-info descriptors. This bug was triggered by very<br />
      fast relays reporting exit-port, entry, and dirreq statistics.<br />
      Reported by Olaf Selke. Bugfix on 0.2.2.1-alpha. Fixes bug 2183.</li>
<li> Publish a router descriptor even if generating an extra-info<br />
      descriptor fails. Previously we would not publish a router<br />
      descriptor without an extra-info descriptor; this can cause fast<br />
      exit relays collecting exit-port statistics to drop from the<br />
      consensus. Bugfix on 0.1.2.9-rc; fixes bug 2195.</li>
</ul>

