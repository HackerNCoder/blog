title: New Release: Tor Browser 10.5a7
---
pub_date: 2021-01-20
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
_html_body:

<p>Tor Browser 10.5a7 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a7/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-1008">latest stable release</a> instead.</p>
<p>This release updates Firefox to 78.6.1esr for desktop and Firefox for Android to 85.0.0-beta.7. Additionally, we update Tor to 0.4.5.3-rc. This versions also fixes a crash seen by macOS users on the new M1 processor.</p>
<p><b>Note:</b> We are <a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40208">investigating</a> a build reproducibility issue for the Android packages. We identified where the packages from different builders differ and we are working on a fix for the next version.</p>
<p><b>Note:</b> We are aware that default bridges are not currently working in recent Tor Browser Alpha versions and Tor Browser does not start, as a result. The issue is being actively <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40282">investigated</a>.</p>
<p><b>Update:</b> The issue impacting default bridges should now be resolved in recent Tor Browser Nightly versions.</p>
<p><strong>Note:</strong> Tor Browser 10.5 <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40089">does not support CentOS 6</a>.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a6 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update NoScript to 11.1.8</li>
<li><a href="https://bugs.torproject.org/tpo/applications/40204/40204">Bug 40204</a>: Update Tor to 0.4.5.3-rc</li>
<li>Translations update</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.6.1esr</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40287">Bug 40287</a>: Switch DDG search from POST to GET</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40297">Bug 40297</a>: Rebase 10.5 patches onto 78.6.1esr</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40036">Bug 40036</a>: Rebase patches onto v70.0.11</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40134">Bug 40134</a>: Rebase patches onto v85.0.0-beta.7</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40293">Bug 40293</a>: Rebase patches onto 85.0b9-build1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40165">Bug 40165</a>: Update zstd to 1.4.8</li>
</ul>
</li>
<li>OS X
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40262">Bug 40262</a>: Browser tabs crashing on the new Macbooks with the M1 chip</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40194">Bug 40194</a>: Remove osname part in cbindgen filename</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40162">Bug 40162</a>: Build Fenix instrumented tests apk</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40190">Bug 40190</a>: Update toolchain for Fenix 85</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40191">Bug 40191</a>: Update Fenix and dependencies to 85.0.0-beta1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40193">Bug 40193</a>: Build all mobile Rust targets in a single step</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40195">Bug 40195</a>: repo.spring.io is not usable anymore</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-290937"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290937" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Concerned Tor User (not verified)</span> said:</p>
      <p class="date-time">January 19, 2021</p>
    </div>
    <a href="#comment-290937">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290937" class="permalink" rel="bookmark">&quot;Bug 40287 fix&quot; (Switch DDG…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Bug 40287 fix" (Switch DDG search from POST to GET) is a hit on our privacy and an errosion of anonymity of all TorBrowser users, since DDG is the default search engine.</p>
<p>The GET method discloses the search keywords clearly in the URL request, as the URL is not encrypted by means of HTTPS. The Tor encryption is great, but a malicious Tor node can "see" the search queries in clear, and collect information on Tor users, in time leading to corellation and deanonymization. I protest.</p>
<p>Even the justifying reason for this "improvement fix" is quite weak (see <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40287" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/402…</a> ):<br />
To help that coder, simply tell him/her to open the needed links as the browser's New Tabs while keeping the original search results Tab until finished!<br />
Is not this a natural solution we've already came to use while browsing?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290943"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290943" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">January 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290937" class="permalink" rel="bookmark">&quot;Bug 40287 fix&quot; (Switch DDG…</a> by <span>Concerned Tor User (not verified)</span></p>
    <a href="#comment-290943">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290943" class="permalink" rel="bookmark">That is not correct. The…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That is not correct. The path and query strings are encrypted by HTTPS. When using HTTPS (which is enforced for duckduckgo.com) the url is not visible to the exit node. We are assuming that DuckDuckGo logs all post requests, so the POST method does not provide any additional security or privacy over GET, and it only hurts the usability of Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290964"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290964" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Anonymous IT Security Guy">Anonymous IT S… (not verified)</span> said:</p>
      <p class="date-time">January 22, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-290964">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290964" class="permalink" rel="bookmark">In my TB (10.0.8), when I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In my TB (10.0.8), when I click back in the DDG search scenario described above, a box pops up asking if I want to "Resend" (do the POST again) because the results are no longer cached.</p>
<p>I think it is probably not that big of a deal from a privacy perspective. There is a difference that POSTed search terms don't get logged in detail in webserver logs because the search terms are not part of the URL, so they are less visible to an attacker who manages to get the URL logs. However, that is not the threat model being described, and it is muted by other privacy protections built into Tor Browser while using Tor.</p>
<p>On that note, it would be nice if you could bring back DuckDuckGo SSL Lite and DuckDuckGo Onion SSL Lite search engines. I modify my search.json.mozlz4 file to use those non-javascript versions - they render much faster on slower hardware, although there is some functionality loss without javascript.</p>
<p>Also, thanks to the Tor Browser and Tor teams for all of your hard work! :-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290996"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290996" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 27, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290964" class="permalink" rel="bookmark">In my TB (10.0.8), when I…</a> by <span content="Anonymous IT Security Guy">Anonymous IT S… (not verified)</span></p>
    <a href="#comment-290996">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290996" class="permalink" rel="bookmark">&gt; DuckDuckGo SSL Lite and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; DuckDuckGo SSL Lite and DuckDuckGo Onion SSL Lite</p>
<p>Variations of the same engine increase the entropy (uniqueness, trackability) of your browser fingerprint. If you want non-javascript, use Safest security level, and DDG will redirect to those non-JS versions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290981"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290981" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-290981">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290981" class="permalink" rel="bookmark">From the Answer #12 here:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From the Answer #12 here: <a href="https://security.stackexchange.com/questions/7705/does-ssl-tls-https-hide-the-urls-being-accessed" rel="nofollow">https://security.stackexchange.com/questions/7705/does-ssl-tls-https-hide-the-urls-being-accessed</a></p>
<blockquote><p>Yes and no.</p>
<p>The url is encrypted properly, so query parameters should never be revealed directly.<br />
However, traffic analysis can get the length of the URL often - and knowing the server and the length of the url is often enough to eavesdrop what pages are being accessed, especially if assuming that links on a page are clicked.<br />
Google for "traffic analysis ssl browsing" or something similar if the topic interests you. [...]</p></blockquote>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291180"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291180" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290981" class="permalink" rel="bookmark">From the Answer #12 here:…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291180">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291180" class="permalink" rel="bookmark">That answer is a little…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That answer is a little deceiving. That information is similarly included when you send a query via POST. The only real difference between GET and POST from the perspective of a network adversary is the location of the query within the message.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-290952"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290952" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 21, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290937" class="permalink" rel="bookmark">&quot;Bug 40287 fix&quot; (Switch DDG…</a> by <span>Concerned Tor User (not verified)</span></p>
    <a href="#comment-290952">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290952" class="permalink" rel="bookmark">URL is not encrypted by…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>URL is not encrypted by means of HTTPS</p></blockquote>
<p>yes it is?<br />
If you do any DDG search directly on the website, that already uses a GET request. GET and POST don't differ when it comes to encryption. In this context the only difference between them is your search terms appearing in the url.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291022"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291022" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 29, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290937" class="permalink" rel="bookmark">&quot;Bug 40287 fix&quot; (Switch DDG…</a> by <span>Concerned Tor User (not verified)</span></p>
    <a href="#comment-291022">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291022" class="permalink" rel="bookmark">&gt; The GET method discloses…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; The GET method discloses the search keywords clearly in the URL request</p>
<p>That bit is true. Parameters such as query keywords are disclosed to the client's device and to the web server, but thanks to HTTPS which envelopes both GET and POST, parameters are not disclosed to eavesdroppers. As alluded by "Anonymous IT Security Guy" and the coder on gitlab, POST interferes with the Back button and with saving parameters in URLs of the client's bookmarks and history, but POST makes parameters a bit harder to log by the web server.</p>
<p>Hits on privacy of GET and POST in HTTPS are based on who controls the logging capability. Software is able to log URLs which include parameters sent via GET, but it's trivial to log parameters sent via POST. Tor Browser defaults to not saving history to disk, but it allows users to save bookmarks to disk at their own risk. So in Tor Browser, POST improves privacy of the bookmarks of that search engine on the client's device at the cost of degrading usability, but it does not assuredly improve privacy of logs on the server.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290938"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290938" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>opaipa (not verified)</span> said:</p>
      <p class="date-time">January 19, 2021</p>
    </div>
    <a href="#comment-290938">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290938" class="permalink" rel="bookmark">Downloaded 10.5a7 - win10…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Downloaded 10.5a7 - win10 pro 64b</p>
<p>- Installed portable/new: after configurating bridge (obfs4) - TOR doesn't start.<br />
- Updating portable 10.5a4 - TOR doesn't start</p>
<p>Log:<br />
1/20/21, 07:03:13.305 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
1/20/21, 07:03:13.305 [WARN] Bridge line with transport obfs4 is missing a ClientTransportPlugin line<br />
1/20/21, 07:03:13.305 [ERR] set_options(): Bug: Acting on config options left us in a broken state. Dying. (on Tor 0.4.5.3-rc e5c47d295bd3dc35)</p>
<p>As written yesterday - please don't make this version broadly available - stop and retract/withdraw from distribution channels - fix this nogo-bug first!<br />
What's up with (automated) testing?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290942" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">January 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290938" class="permalink" rel="bookmark">Downloaded 10.5a7 - win10…</a> by <span>opaipa (not verified)</span></p>
    <a href="#comment-290942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290942" class="permalink" rel="bookmark">I&#039;ll update the blog post…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'll update the blog post and mention this. That problem is already being investigated: <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40282" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/402…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290945"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290945" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>opaipa (not verified)</span> said:</p>
      <p class="date-time">January 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-290945">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290945" class="permalink" rel="bookmark">Thank you,
seems more…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you,<br />
seems more complicated than expected. It also evaded testsuites...</p>
<p>Re: "Let's try remembering this for 10.5a8."<br />
Would you mind writing/telling as soon as there are nightlies which you are assuming to fix this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290947"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290947" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">January 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290945" class="permalink" rel="bookmark">Thank you,
seems more…</a> by <span>opaipa (not verified)</span></p>
    <a href="#comment-290947">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290947" class="permalink" rel="bookmark">Yes, we&#039;ll update this blog…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, we'll update this blog post when the fix is available.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290949"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290949" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>opaipa (not verified)</span> said:</p>
      <p class="date-time">January 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-290949">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290949" class="permalink" rel="bookmark">Thanx
Yes, it&#039;s alpha, but…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanx<br />
Yes, it's alpha, but since the update ruins existing configurations (rendering TBa unusable)  - shouldn't you stop the update process until there's is a viable solution for updating?<br />
And/or add a red CAVEAT at the announcing blog-statement above?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-290944"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290944" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>abcde (not verified)</span> said:</p>
      <p class="date-time">January 20, 2021</p>
    </div>
    <a href="#comment-290944">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290944" class="permalink" rel="bookmark">The &quot;investigated&quot;-link in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The "investigated"-link in the blog post does not work because of extra "https//".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290946"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290946" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">January 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290944" class="permalink" rel="bookmark">The &quot;investigated&quot;-link in…</a> by <span>abcde (not verified)</span></p>
    <a href="#comment-290946">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290946" class="permalink" rel="bookmark">Thanks! Fixed.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks! Fixed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290959" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Tyler (not verified)</span> said:</p>
      <p class="date-time">January 22, 2021</p>
    </div>
    <a href="#comment-290959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290959" class="permalink" rel="bookmark">The .dmg download links for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The .dmg download links for most of the MacOS releases do not exist, there is only the one for TorBrowser-10.5a7-osx64_ar.dmg.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290980"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290980" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Russian user (not verified)</span> said:</p>
      <p class="date-time">January 25, 2021</p>
    </div>
    <a href="#comment-290980">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290980" class="permalink" rel="bookmark">https://dist.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://dist.torproject.org/torbrowser/10.5a7/tor-browser-10.5a7-android-aarch64-androidTest.apk" rel="nofollow">https://dist.torproject.org/torbrowser/10.5a7/tor-browser-10.5a7-androi…</a></p>
<p>What's this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290989"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290989" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290980" class="permalink" rel="bookmark">https://dist.torproject.org…</a> by <span>Russian user (not verified)</span></p>
    <a href="#comment-290989">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290989" class="permalink" rel="bookmark">That is a new apk created…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That is a new apk created during the build process that we use for testing. It isn't an apk you should install.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290984"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290984" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>whois (not verified)</span> said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>
    <a href="#comment-290984">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290984" class="permalink" rel="bookmark">Hello!
downloaded a file…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello!<br />
downloaded a file named "tor-browser-10.5a7-android-aarch64-androidTest.apk" from your storage.<br />
Link to the file:<br />
<a href="https://dist.torproject.org/torbrowser/10.5a7/tor-browser-10.5a7-android-aarch64-androidTest.apk" rel="nofollow">https://dist.torproject.org/torbrowser/10.5a7/tor-browser-10.5a7-androi…</a></p>
<p>What's this? The Virus?</p>
</div>
  </div>
</article>
<!-- Comment END -->
