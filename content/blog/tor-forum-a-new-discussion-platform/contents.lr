title: Tor Forum: a new discussion platform for the Tor Community
---
pub_date: 2021-11-01
---
author: ggus
---
_html_body:

<p>Communicating and finding help online is crucial to building a solid community. After many years of using emails, mailing lists, blog comments, and IRC to help Tor users, we believe that time has come to improve our discussion channels.</p>
<p>Today, we're happy to announce a new discussion and user support platform: the <a href="https://forum.torproject.net">Tor Forum</a>.</p>
<p>The new forum is powered by <a href="https://meta.discourse.org/">Discourse</a>: a modern, friendly, and free and open source software. The forum posts are publicly readable, and you don't need to log in to navigate and access the content. It's also possible to install the Discourse App on your mobile device and receive notifications. For users who like the traditional mailing list format, Discourse features email integration. The new forum is compatible and works with Tor Browser (security slider level set <a href="https://tb-manual.torproject.org/security-settings/">'Safer'</a>).</p>
<p>Currently, the Tor Forum is fully hosted by Discourse, and because they do not support onion services yet, it won't have an onion site soon. That is also why the domain is torproject.net, because of our <a href="https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-6-naming-convention">system security policy</a> on using *.torproject.org only for sites we host in our own infrastructure.</p>
<p>In the last few years, the Tor Project has improved internal tools and platforms to make it easy to contribute to Tor software development and to participate in our community. For example, we <a href="https://blog.torproject.org/meet-new-torprojectorg">revamped our websites</a>, <a href="https://blog.torproject.org/node/1957">moved from Trac to GitLab</a>, <a href="https://blog.torproject.org/entering-the-matrix">connected our chat channels on Matrix</a>, and now we're launching the Tor Forum.</p>
<p>We invite the Tor community to join the Tor Forum and contribute with us!</p>
<h3>Moderation policy: Don't be a jerk. Be awesome instead.</h3>
<p>All discussions on the Tor Forum will follow the Tor Project's <a href="https://forum.torproject.net/t/welcome-to-the-tor-project-forum/">Code of Conduct</a>. Before creating an account, users should read and agree with the <a href="https://forum.torproject.net/t/welcome-to-the-tor-project-forum/7">moderation policy and our Code of Conduct</a>. Moderators will enforce this policy to keep our community open, welcoming, and inclusive. Keeping a community healthy and welcoming requires an active community and moderation. Users with higher <a href="https://meta.discourse.org/t/understanding-discourse-trust-levels/">trust levels</a> will be able to flag harmful posts and report to moderators.</p>
<h3>The future of mailing lists, blog comments, and Frontdesk</h3>
<p>Multiple user support and feedback channels makes it difficult for users to find information and ask questions in a venue where they will receive timely and accurate answers. Plus, maintaining disconnected channels is unsustainable for Tor staff. Both of these challenges can be addressed in the same way: by modernizing and streamlining our support and feedback channels. The Tor Forum will begin to take the place of some of these spaces.</p>
<p>At the beginning of 2022, we will deprecate and archive some of the unused and unmaintained mailing lists and move these discussions to the Tor Forum. The forum has the capacity to receive mails from specific categories and reply through a mail client. Popular and functional mailing lists like <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev">tor-dev</a> and <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-project">tor-project</a> won't be archived and will continue to exist.</p>
<p>Soon, we will migrate the <a href="https://blog.torproject.org/">Tor Blog</a> to a static generated website, Lektor. The blog comments will be hosted on Discourse in the new Tor Forum setup. This will make the comment section of each blog post easier to maintain and more open, as moderation will be distributed among trusted Tor Forum users.</p>
<p>Additionally, we will soon begin to inform/point users who come to our <a href="mailto:frontdesk@torproject.org">frontdesk@torproject.org</a> email service to a public resource. For users living in censored countries where the Tor Project domains are blocked, like China, Iran, Turkey, Egypt, and others, we'll continue providing support by email and <a href="https://support.torproject.org/get-in-touch/irc-help/">chat</a>.</p>
<h3>Introducing the Tor Forum categories</h3>
<p>We are starting the Tor Forum with a few categories. Depending on the forum usage, we can edit, remove, and create new categories. At the moment, you will find these discussion categories:</p>
<h4>1. <a href="https://forum.torproject.net/c/feedback/">Feedback</a></h4>
<p>Most people use Tor with Tor Browser, and we would love to know how we can improve the user experience and/or user interface. This section is for feedback about <a href="https://forum.torproject.net/c/feedback/tor-browser-feedback/7">Tor Browser</a>, <a href="https://forum.torproject.net/c/feedback/tor-browser-alpha-feedback/">Tor Browser Alpha</a>, the <a href="https://forum.torproject.net/c/feedback/websites-feedback/">Tor Project Websites</a>, <a href="https://forum.torproject.net/c/feedback/localization-feedback/">Localization</a>, and our own <a href="https://forum.torproject.net/c/feedback/forum-feedback/">Forum</a>. User feedback is limited to products and tools developed by the Tor Project.</p>
<h4>2. <a href="https://forum.torproject.net/c/support/12">Support</a></h4>
<p>Whether you're using <a href="https://forum.torproject.net/c/support/tor-browser-for-android/">Tor Browser for Android</a>, <a href="https://forum.torproject.net/c/support/relay-operator/">setting up a relay</a>, or <a href="https://forum.torproject.net/c/support/censorship-circumvention/">trying to circumvent censorship</a>, you can find help in this category.</p>
<p>Remember that you should check their support channels for help with Tails, OnionShare, Ricochet Refresh, and other tools. User support is limited to products and tools developed by the Tor Project.</p>
<h4>3. <a href="https://forum.torproject.net/c/in-your-language/">In your language</a></h4>
<p>If you want to write about Tor in your language, have nice articles to suggest, or simply want to ask a question, please comment on this category.</p>
<h4>4. <a href="https://forum.torproject.net/c/events/">Events</a></h4>
<p>A list of upcoming Tor events. (This category might get busier again after the pandemic.)</p>
<h4>5. <a href="https://forum.torproject.net/c/mailing-lists/">Mailing lists</a></h4>
<p>We've mirrored two mailing lists (tor-project and tor-relays), and users can read and receive notifications on Discourse. It's convenient to read the mailing lists without being subscribed to them using the Discourse App. But, on the other hand, it's just a read-only mirror. Users can't reply to mailing lists using Discourse.</p>
<h4>6. <a href="https://forum.torproject.net/c/general-discussion/">General Discussion</a></h4>
<div class="cooked">
<p>Anything related to Tor ecosystem which does not fit into the Support or feedback category</p>
</div>
<h4>How to join the Tor Forum</h4>
<p>1. Visit the Tor Forum website: <a href="https://forum.torproject.net">https://forum.torproject.net</a></p>
<p>2. Click on "Sign up" and register your account. A valid email is required.</p>
<p>3. You will receive an automatic email from <a href="mailto:torproject1@discoursemail.com">torproject1@discoursemail.com</a> with a verification link. Click on this link to finish your registration.</p>
<p>Or you can login using your GitHub or Discord account.</p>
<p>We look forward to seeing you there!</p>

---
_comments:

<a id="comment-293238"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293238" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
    <a href="#comment-293238">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293238" class="permalink" rel="bookmark">Why aren&#039;t you self-hosting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why aren't you self-hosting the forum? I know discourse is a horrifying piece of software to self-host, but surely this should've given you pause?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293241"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293241" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293238" class="permalink" rel="bookmark">Why aren&#039;t you self-hosting…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-293241">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293241" class="permalink" rel="bookmark">Indeed. I&#039;m actually very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Indeed. I'm actually very surprised they chose to do it this way. It seems incongruous with the tor project's goals and philosophy.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293258"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293258" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293241" class="permalink" rel="bookmark">Indeed. I&#039;m actually very…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-293258">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293258" class="permalink" rel="bookmark">IRC has never been hosted by…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>IRC has never been hosted by them AFAIK.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-293242"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293242" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293238" class="permalink" rel="bookmark">Why aren&#039;t you self-hosting…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-293242">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293242" class="permalink" rel="bookmark">When we started to work with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When we started to work with the new Discourse Forum, the Tor Project SysAdmin team was under staff capacity.  But, we have plans on moving it to our own infrastructure next year.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-293240"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293240" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
    <a href="#comment-293240">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293240" class="permalink" rel="bookmark">To be honest, I&#039;m really not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To be honest, I'm really not enthused with Discourse. It's an extremely javascript-heavy forum software; difficult to use and more difficult to archive.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293243"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293243" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293240" class="permalink" rel="bookmark">To be honest, I&#039;m really not…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-293243">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293243" class="permalink" rel="bookmark">We know that many of our…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We know that some of our users dislike JS. But you can always enable Discourse's "mailing list" mode and receive all threads and updates in your email client.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293246"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293246" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to Gus</p>
    <a href="#comment-293246">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293246" class="permalink" rel="bookmark">So instead of JS, we would…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So instead of JS, we would share our e-mail and face a different set of issues. Consider Tor Project's <a href="https://community.torproject.org/user-research/persona/" rel="nofollow">personas</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293254"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293254" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293246" class="permalink" rel="bookmark">So instead of JS, we would…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-293254">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293254" class="permalink" rel="bookmark">We&#039;ve considered and that&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We've considered and that's why we will still support censored users in our helpdesk email.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-293247"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293247" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
    <a href="#comment-293247">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293247" class="permalink" rel="bookmark">Wait, wait, wait.
You&#039;re…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wait, wait, wait. You're taking away anonymous commenting? We can't just type a captcha as we do now?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293251"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293251" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293247" class="permalink" rel="bookmark">Wait, wait, wait.
You&#039;re…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-293251">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293251" class="permalink" rel="bookmark">You can create an account…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can create an account using Tor and use your favorite nickname. It's like what we have now for IRC.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293253"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293253" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Comment denied (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to Gus</p>
    <a href="#comment-293253">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293253" class="permalink" rel="bookmark">On a lower safety setting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On a lower safety setting and with email....</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-293259"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293259" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonym (not verified)</span> said:</p>
      <p class="date-time">November 01, 2021</p>
    </div>
    <a href="#comment-293259">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293259" class="permalink" rel="bookmark">&quot;Soon, we will migrate the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Soon, we will migrate the Tor Blog to a static generated website, Lektor."</p>
<p>Why is this a good idea, for the torproject-crew, to close this OPEN blog and move this to an (yet)third-party hosted, Javascript dependent, email-login(!) dependent structure?<br />
Less administrative costs? Bon, it's new.<br />
Someone may like -lol- things like this<br />
<a href="https://nvd.nist.gov/vuln/detail/CVE-2021-41163" rel="nofollow">https://nvd.nist.gov/vuln/detail/CVE-2021-41163</a><br />
"maliciously crafted requests could lead to remote code execution."<br />
But what ist better, for the average user?<br />
Hard to understand not for me only.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293262"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293262" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">November 02, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293259" class="permalink" rel="bookmark">&quot;Soon, we will migrate the…</a> by <span>anonym (not verified)</span></p>
    <a href="#comment-293262">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293262" class="permalink" rel="bookmark">From the blog post: …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From the blog post: "Multiple user support and feedback channels makes it difficult for users to find information and ask questions in a venue where they will receive timely and accurate answers. Plus, maintaining disconnected channels is unsustainable for Tor staff. Both of these challenges can be addressed in the same way: by modernizing and streamlining our support and feedback channels. The Tor Forum will begin to take the place of some of these spaces."</p>
<p>Having a platform like Discourse will help users find the solution for their questions and needs. This blog was certainly helpful, but nowadays, open source projects are just using more modern platforms.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-293263"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293263" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Comment denied  (not verified)</span> said:</p>
      <p class="date-time">November 02, 2021</p>
    </div>
    <a href="#comment-293263">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293263" class="permalink" rel="bookmark">Here is your first &#039;to do&#039;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here is your first 'to do' item from the forum, we've been told to make the developers aware, which raises the question as to why devs aren't browsing the forum themselves? </p>
<p><a href="https://forum.torproject.net/t/accept-launguage-in-user-agent-leaks-device-localization-information/344" rel="nofollow">https://forum.torproject.net/t/accept-launguage-in-user-agent-leaks-dev…</a></p>
<p>Closed gitlab entry from over a year ago, despite zero changes thus leak remaining</p>
<p><a href="https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40087" rel="nofollow">https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40087</a></p>
<p>Also, in your post about privacy being a human right you say the following</p>
<p>"In 2022, we will begin rolling out some of these improvements to the live network, making Tor faster for users, especially if you’re on a mobile device."</p>
<p>Doesn't this mean mobile users will be discernable against all others, thereby creating a smaller attack surface for the bigger boys? Why? I thought the whole point was every device under every/any OS will look forensically identical?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293264"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293264" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">November 02, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-293263" class="permalink" rel="bookmark">Here is your first &#039;to do&#039;…</a> by <span>Comment denied  (not verified)</span></p>
    <a href="#comment-293264">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293264" class="permalink" rel="bookmark">The topic was resolved.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The topic was resolved.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
