title: Tor 0.2.9.4-alpha is released, with important fixes
---
pub_date: 2016-10-17
---
author: nickm
---
_html_body:

<p>Tor 0.2.9.4-alpha fixes a security hole in previous versions of Tor that would allow a remote attacker to crash a Tor client, hidden service, relay, or authority. All Tor users should upgrade to this version, or to 0.2.8.9. Patches will be released for older versions of Tor.<br />
Tor 0.2.9.4-alpha also adds numerous small features and fix-ups to previous versions of Tor, including the implementation of a feature to future- proof the Tor ecosystem against protocol changes, some bug fixes necessary for Tor Browser to use unix domain sockets correctly, and several portability improvements. We anticipate that this will be the last alpha in the Tor 0.2.9 series, and that the next release will be a release candidate.<br />
You can download the source from the usual place on the website. Packages should be available over the next several days. Remember to check the signatures!<br />
Please note: This is an alpha release. You should only try this one if you are interested in tracking Tor development, testing new features, making sure that Tor still builds on unusual platforms, or generally trying to hunt down bugs. If you want a stable experience, please stick to the stable releases.<br />
Below are the changes since 0.2.9.3-alpha.</p>

<h2>Changes in version 0.2.9.4-alpha - 2016-10-17</h2>

<ul>
<li>Major features (security fixes):
<ul>
<li>Prevent a class of security bugs caused by treating the contents of a buffer chunk as if they were a NUL-terminated string. At least one such bug seems to be present in all currently used versions of Tor, and would allow an attacker to remotely crash most Tor instances, especially those compiled with extra compiler hardening. With this defense in place, such bugs can't crash Tor, though we should still fix them as they occur. Closes ticket <a href="https://bugs.torproject.org/20384" rel="nofollow">20384</a> (TROVE-2016-10-001).
  </li>
</ul>
</li>
<li>Major features (subprotocol versions):
<ul>
<li>Tor directory authorities now vote on a set of recommended subprotocol versions, and on a set of required subprotocol versions. Clients and relays that lack support for a _required_ subprotocol version will not start; those that lack support for a _recommended_ subprotocol version will warn the user to upgrade. Closes ticket <a href="https://bugs.torproject.org/19958" rel="nofollow">19958</a>; implements part of proposal 264.
  </li>
<li>Tor now uses "subprotocol versions" to indicate compatibility. Previously, versions of Tor looked at the declared Tor version of a relay to tell whether they could use a given feature. Now, they should be able to rely on its declared subprotocol versions. This change allows compatible implementations of the Tor protocol(s) to exist without pretending to be 100% bug-compatible with particular releases of Tor itself. Closes ticket <a href="https://bugs.torproject.org/19958" rel="nofollow">19958</a>; implements part of proposal 264.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Minor feature (fallback directories):
<ul>
<li>Remove broken fallbacks from the hard-coded fallback directory list. Closes ticket <a href="https://bugs.torproject.org/20190" rel="nofollow">20190</a>; patch by teor.
  </li>
</ul>
</li>
<li>Minor features (client, directory):
<ul>
<li>Since authorities now omit all routers that lack the Running and Valid flags, we assume that any relay listed in the consensus must have those flags. Closes ticket <a href="https://bugs.torproject.org/20001" rel="nofollow">20001</a>; implements part of proposal 272.
  </li>
</ul>
</li>
<li>Minor features (compilation, portability):
<ul>
<li>Compile correctly on MacOS 10.12 (aka "Sierra"). Closes ticket <a href="https://bugs.torproject.org/20241" rel="nofollow">20241</a>.
  </li>
</ul>
</li>
<li>Minor features (development tools, etags):
<ul>
<li>Teach the "make tags" Makefile target how to correctly find "MOCK_IMPL" function definitions. Patch from nherring; closes ticket <a href="https://bugs.torproject.org/16869" rel="nofollow">16869</a>.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the October 4 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (unix domain sockets):
<ul>
<li>When configuring a unix domain socket for a SocksPort, ControlPort, or Hidden service, you can now wrap the address in quotes, using C-style escapes inside the quotes. This allows unix domain socket paths to contain spaces.
  </li>
</ul>
</li>
<li>Minor features (virtual addresses):
<ul>
<li>Increase the maximum number of bits for the IPv6 virtual network prefix from 16 to 104. In this way, the condition for address allocation is less restrictive. Closes ticket <a href="https://bugs.torproject.org/20151" rel="nofollow">20151</a>; feature on 0.2.4.7-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (address discovery):
<ul>
<li>Stop reordering IP addresses returned by the OS. This makes it more likely that Tor will guess the same relay IP address every time. Fixes issue 20163; bugfix on 0.2.7.1-alpha, ticket <a href="https://bugs.torproject.org/17027" rel="nofollow">17027</a>. Reported by René Mayrhofer, patch by "cypherpunks".
  </li>
</ul>
</li>
<li>Minor bugfixes (client, unix domain sockets):
<ul>
<li>Disable IsolateClientAddr when using AF_UNIX backed SocksPorts as the client address is meaningless. Fixes bug <a href="https://bugs.torproject.org/20261" rel="nofollow">20261</a>; bugfix on 0.2.6.3-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (compilation, OpenBSD):
<ul>
<li>Detect Libevent2 functions correctly on systems that provide libevent2, but where libevent1 is linked with -levent. Fixes bug <a href="https://bugs.torproject.org/19904" rel="nofollow">19904</a>; bugfix on 0.2.2.24-alpha. Patch from Rubiate.
  </li>
</ul>
</li>
<li>Minor bugfixes (configuration):
<ul>
<li>When parsing quoted configuration values from the torrc file, handle windows line endings correctly. Fixes bug <a href="https://bugs.torproject.org/19167" rel="nofollow">19167</a>; bugfix on 0.2.0.16-alpha. Patch from "Pingl".
  </li>
</ul>
</li>
<li>Minor bugfixes (getpass):
<ul>
<li>Defensively fix a non-triggerable heap corruption at do_getpass() to protect ourselves from mistakes in the future. Fixes bug #19223; bugfix on 0.2.7.3-rc. Bug found by Guido Vranken, patch by nherring.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden service):
<ul>
<li>Allow hidden services to run on IPv6 addresses even when the IPv6Exit option is not set. Fixes bug <a href="https://bugs.torproject.org/18357" rel="nofollow">18357</a>; bugfix on 0.2.4.7-alpha.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Add module-level internal documentation for 36 C files that previously didn't have a high-level overview. Closes ticket #20385.
  </li>
</ul>
</li>
<li>Required libraries:
<ul>
<li>When building with OpenSSL, Tor now requires version 1.0.1 or later. OpenSSL 1.0.0 and earlier are no longer supported by the OpenSSL team, and should not be used. Closes ticket <a href="https://bugs.torproject.org/20303" rel="nofollow">20303</a>.
  </li>
</ul>
</li>
</ul>

