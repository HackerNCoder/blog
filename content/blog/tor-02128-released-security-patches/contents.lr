title: Tor 0.2.1.28 is released (security patches)
---
pub_date: 2010-12-20
---
author: phobos
---
tags:

security advisory
bug fixes
enhancements
---
categories: releases
---
_html_body:

<p>Tor 0.2.1.28 does some code cleanup to reduce the risk of remotely<br />
exploitable bugs. Thanks to Willem Pinckaers for notifying us of the<br />
issue. The Common Vulnerabilities and Exposures project has assigned<br />
CVE-2010-1676 to this issue.</p>

<p>We also took this opportunity to change the IP address for one of our<br />
directory authorities, and to update the geoip database we ship.</p>

<p>All Tor users should upgrade.</p>

<p><a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a></p>

<p>Changes in version 0.2.1.28 - 2010-12-17<br />
<strong>Major bugfixes:</strong></p>

<ul>
<li>Fix a remotely exploitable bug that could be used to crash instances of Tor remotely by overflowing on the heap. Remote-code execution hasn't been confirmed, but can't be ruled out. Everyone should  upgrade. Bugfix on the 0.1.1 series and later.
</li>
</ul>

<p><strong>Directory authority changes:</strong></p>

<ul>
<li>Change IP address and ports for gabelmoo (v3 directory authority).</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Update to the December 1 2010 Maxmind GeoLite Country database.</li>
</ul>

